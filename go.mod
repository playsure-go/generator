module bitbucket.org/playsure-go/generator

go 1.20

require (
	bitbucket.org/playsure-go/hash v0.1.0
	github.com/google/uuid v1.3.1
)

require golang.org/x/crypto v0.11.0 // indirect
