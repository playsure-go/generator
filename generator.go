package generator

import (
	"bitbucket.org/playsure-go/hash"
	"fmt"
	"github.com/google/uuid"
	gorand "math/rand"
	"strings"
	"time"
)

var rand *gorand.Rand

func init() {
	rand = gorand.New(gorand.NewSource(time.Now().UnixNano()))
}

func Bytes(length int) []byte {
	if length < 0 {
		length = 0
	}
	bs := make([]byte, length)
	for i := 0; i < length; i++ {
		bs[i] = byte(rand.Intn(256))
	}
	return bs
}

func Password(chars string, length int) string {
	a := []rune(chars)
	lenA := len(a)
	var b strings.Builder
	for i := 0; i < length; i++ {
		b.WriteRune(a[rand.Intn(lenA)])
	}
	return b.String()
}

func Md5() string {
	s := randomSource()
	return hash.Md5(s)
}

func Sha1() string {
	s := randomSource()
	return hash.Sha1(s)
}

func Sha256() string {
	s := randomSource()
	return hash.Sha256(s)
}

func Sha512() string {
	s := randomSource()
	return hash.Sha512(s)
}

func Uuid() string {
	u := uuid.New()
	return u.String()
}

func randomSource() string {
	r1 := rand.Uint64()
	t := uint64(time.Now().UnixNano())
	r2 := rand.Uint64()
	p := Password("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 16)
	s := fmt.Sprintf("%016x%016x%016X%16s", r1, t, r2, p) // Length = 16 * 4 = 64.
	return s
}
